package scrape

type Result struct {
	ImageUrl []string
	Url string
	HtmlBody string
}

type ScrapperObject{
	Url string
}
type Scrapper interface {
	func New(url string)
	scrape() []Result
}

